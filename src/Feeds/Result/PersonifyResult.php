<?php

namespace Drupal\feeds_personify\Feeds\Result;

use \Drupal\feeds\Result\HttpFetcherResult;
use \Drupal\Component\Render\FormattableMarkup;

/**
 * Personify Result
 */
class PersonifyResult extends HttpFetcherResult {

    /**
     * Checks that a file exists and is readable.
     *
     * @throws \RuntimeException
     *   Thrown if the file isn't readable or writable.
     */
    protected function checkFile() {
        if (!is_readable($this->filePath)) {
            throw new \RuntimeException(new FormattableMarkup('File %filepath is not readable.', ['%filepath' => $this->filePath]));
        }
    }

}
