<?php

namespace Drupal\feeds_personify\Feeds\Item;

use Drupal\feeds\Feeds\Item\DynamicItem;

/**
 * Defines an item class
 */
class PersonifyItem extends DynamicItem {

    protected $data = [];

}