<?php

namespace Drupal\feeds_personify\Feeds\Parser;

use Drupal\feeds\Component\XmlParserTrait;
use Drupal\feeds\Exception\EmptyFeedException;
use Drupal\feeds\FeedInterface;
use Drupal\feeds\Plugin\Type\Parser\ParserInterface;
use Drupal\feeds\Plugin\Type\PluginBase;
use Drupal\feeds\Result\FetcherResultInterface;
use Drupal\feeds\Result\ParserResult;
use Drupal\feeds\StateInterface;
use Drupal\feeds_personify\Feeds\Item\PersonifyItem;

/**
 * Defines a parser.
 *
 * @FeedsParser(
 *   id = "personify",
 *   title = @Translation("Personify Parser"),
 *   description = @Translation("Parse Personify XML."),
 *   form = {
 *     "configuration" = "Drupal\feeds_personify\Feeds\Parser\Form\PersonifyParserForm",
 *   },
 * )
 */
class PersonifyParser extends PluginBase implements ParserInterface {
  use XmlParserTrait;

  /**
   * {@inheritdoc}
   */
  public function parse(FeedInterface $feed, FetcherResultInterface $fetcher_result, StateInterface $state) {

    // Set time zone to GMT for parsing dates with strtotime().
    $tz = date_default_timezone_get();
    date_default_timezone_set('GMT');

    $raw = trim($fetcher_result->getRaw());
    if (!strlen($raw)) {
      throw new EmptyFeedException();
    }

    static::startXmlErrorHandling();
    $xml = new \SimpleXMLElement(str_replace('&#x2;','',$raw));
    static::stopXmlErrorHandling();
    $result = new ParserResult();

    // Loop over entries
    foreach ($xml->entry as $key => $val) {

        // Loop over fields
        $item = new PersonifyItem();
        foreach($val->children() as $xmlFieldKey => $xmlFieldValue) {

            // Simple field
            if($xmlFieldKey != 'content') {
                if(isset(json_decode(json_encode($xmlFieldValue))->{0})) {
                    $xmlFieldValue = json_decode(json_encode($xmlFieldValue))->{0};
                }else{
                    $xmlFieldValue = json_decode(json_encode($xmlFieldValue));
                }
                $item->set($xmlFieldKey, $xmlFieldValue);
            }
            // The content field has sub fields
            else {
                $xmlContentFields = json_decode(json_encode($xmlFieldValue->children('m',TRUE)->children('d',TRUE)));
                foreach($xmlContentFields as $xmlContentFieldKey => $xmlContentFieldValue) {
                    if(is_object($xmlContentFieldValue)) {
                        $xmlContentFieldValue = '';
                    }
                    $item->set($xmlContentFieldKey, $xmlContentFieldValue);
                }

            }

        }

        // If there is an updated date set it to null
        // Every time the endpoint is called it has a new date here causing
        // the entity to be saved every time even if no real content has been updated
        if(key_exists('updated', $item->toArray())) {
          $item->set('updated', null);
        }

        $result->addItem($item);

    }


    date_default_timezone_set($tz);
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getMappingSources() {

    $availableFields = unserialize($this->getConfiguration('available_fields'));

    if($availableFields == '') {
        drupal_set_message('Please update parser config.');
        $availableFields = [];
    }

    return $availableFields;

  }

  /**
  * {@inheritdoc}
  */
  public function defaultConfiguration(){
      return [
          'available_fields' => ''
      ];
  }


  public function getFields(){
      $availableFields = array();

      // Check selected endpoint to get fields if the fetcher defined the personify vars
      $fetcherConfiguration = $this->feedType->getFetcher()->configuration;

      // If the connection details were configured in the fetcher
      if(
          isset($fetcherConfiguration['personify_data_service_url'])
          && isset($fetcherConfiguration['specific_endpoint'])
          && isset($fetcherConfiguration['username'])
          && isset($fetcherConfiguration['password'])
      ) {

          // Make a request to the endpoint to get the available fields
          $requestURL = $fetcherConfiguration['personify_data_service_url'];
          if($fetcherConfiguration['specific_endpoint']!='_select') {
              $requestURL = $requestURL.'/'.$fetcherConfiguration['specific_endpoint'];
          }


          try {
              $client = \Drupal::httpClient();
              $guzzleRequestOptions = array(
                  'http_errors' => false,
                  'auth' => [
                      $fetcherConfiguration['username'],
                      $fetcherConfiguration['password']
                  ]
              );
              $response = $client->get($requestURL,$guzzleRequestOptions);
              $responseBody = (String)$response->getBody();
          } catch (RequestException $e) {
              watchdog_exception('feeds_personify', $e->getMessage());
          }

          // Parse the response
          static::startXmlErrorHandling();
          $responseXmlObj = new \SimpleXMLElement($responseBody);
          static::stopXmlErrorHandling();

          foreach ($responseXmlObj->entry->children() as $key => $val) {

              // The content tag we'll treat differently
              if($key != 'content') {
                  $availableFields[$key] = array(
                      'label' => 'Personify: '.$key,
                      //'description' => 'I.E: '.(string)$val,
                      'description' => $key,
                  );
              }else {
                  $xmlFields = json_decode(json_encode($val->children('m',TRUE)->children('d',TRUE)));
                  foreach($xmlFields as $xmlFieldKey => $xmlFieldValue) {
                      $availableFields[$xmlFieldKey] = array(
                          'label' => 'Personify: Content['.$xmlFieldKey.']',
                          //'description' => 'I.E: '.(string)$val,
                          'description' => $xmlFieldKey,
                      );
                  }

              }

          }

      }

      return $availableFields;
  }

}
