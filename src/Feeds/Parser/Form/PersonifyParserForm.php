<?php

namespace Drupal\feeds_personify\Feeds\Parser\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\feeds\Plugin\Type\ExternalPluginFormBase;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;

/**
 * The configuration form for the parser.
 */
class PersonifyParserForm extends ExternalPluginFormBase
{

    /**
     * {@inheritdoc}
     */
    public function buildConfigurationForm(array $form, FormStateInterface $form_state)
    {

        // Markup explaining our methodology here
        $form['#markup'] = '<p>After entering fetcher settings and saving feed type use this button to fetch fields available in selected endpoint.</p>';
        $form['#markup'] .= '<p>You can also update the fields here should they change at any point.</p>';
        $form['#markup'] .= '<b id="parcer-status" style="color:red"></b>';

        // Hidden form field to store fields available from api endpoint
        $form['available_fields'] = [
            '#type' => 'hidden',
            '#default_value' => $this->plugin->getConfiguration('available_fields'),
        ];

        // Ajax button to fetch available fields
        $form['fetch_fields'] = [
            '#type' => 'button',
            '#title' => $this->t('Update available fields for mapping.'),
            '#value' => 'Fetch fields for mapping',
            '#ajax' => [
                'callback' => array($this, 'updateAvailableFields'),
                'event' => 'mouseup',
                'progress' => [
                    'type' => 'throbber',
                    'message' => t('Fetching fields...'),
                ],
            ],
        ];


        return $form;
    }


    /**
     * Function to update available fields for mapping
     * @param array $form
     * @param FormStateInterface $form_state
     * @return AjaxResponse
     */
    public function updateAvailableFields(array &$form, FormStateInterface $form_state){

        // Get fields and make hidden field to hold them and save into config
        $availableFields = $this->getFields();
        $availableFieldFormItem['available_fields'] = [
            '#type' => 'hidden',
            '#value' => $availableFields,
            '#name' => 'parser_configuration[available_fields]',
            '#validated' => TRUE,
        ];
        $ajaxResponse = new AjaxResponse();
        $renderer = \Drupal::service('renderer');
        $ajaxResponse->addCommand(new ReplaceCommand('input[data-drupal-selector=\'edit-parser-configuration-available-fields\']', $renderer->render($availableFieldFormItem)));

        // Give an indication of how it went
        if(strlen($availableFields) > 6) {
            $statusResponse = ['#markup' => '<b id="parcer-status">Sucessfully obtained fields, please save form.</b>'];
        }else {
            $statusResponse = ['#markup' => '<b id="parcer-status">Could not load fields.</b>'];
        }
        $ajaxResponse->addCommand(new ReplaceCommand('#parcer-status', $renderer->render($statusResponse)));

        return $ajaxResponse;

    }

    /**
     * Get fields from endpoint using Parser class
     * @return string
     */
    public function getFields(){
        // Check selected endpoint to get fields if the fetcher defined the personify vars
        return serialize($this->plugin->getFields());
    }

}
