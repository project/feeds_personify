<?php

namespace Drupal\feeds_personify\Feeds\Fetcher\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\feeds\Plugin\Type\ExternalPluginFormBase;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;

/**
 * The configuration form for http fetchers.
 */
class PersonifyFetcherForm extends ExternalPluginFormBase
{

    /**
     * {@inheritdoc}
     */
    public function buildConfigurationForm(array $form, FormStateInterface $form_state)
    {

        // Data Service url
        $form['personify_data_service_url'] = [
            '#title' => $this->t('Personify data service url'),
            '#description' => $this->t('ie https://domain.com/PersonifyDataServices/PersonifyData.svc'),
            '#type' => 'url',
            '#default_value' => $this->plugin->getConfiguration('personify_data_service_url'),
            '#maxlength' => 2048,
            '#required' => TRUE,
        ];

        // Username
        $form['username'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Username'),
            '#description' => $this->t('Personify Username'),
            '#maxlength' => 20,
            '#required' => TRUE,
            '#default_value' => $this->plugin->getConfiguration('username'),
        ];

        // Password
        $form['password'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Password'),
            '#description' => $this->t('Personify Password'),
            '#maxlength' => 20,
            '#required' => TRUE,
            '#default_value' => $this->plugin->getConfiguration('password'),
        ];

        // Specific Endpoint
        $endpointOptions = array('_select'=>'Please select');
        $savedEndpointSelection = $this->plugin->getConfiguration('specific_endpoint');
        if($savedEndpointSelection != '') {
            $endpointOptions = array($savedEndpointSelection => $savedEndpointSelection);
        }
        $form['specific_endpoint'] = [
            '#type' => 'select',
            '#title' => $this->t('Specific Endpoint'),
            '#description' => $this->t('Select the endpoint (collection) you would like to use. Alternately leave blank and enter the full url above.'),
            '#options' => $endpointOptions,
            '#default_value' => $savedEndpointSelection,
            '#validated' => TRUE,
        ];


        // Specific Endpoint Button
        $form['specific_endpoint_button'] = [
            '#type' => 'button',
            '#title' => $this->t('Specific Endpoint'),
            '#value' => 'Load endpoints from Data Service Url',
            '#ajax' => [
                'callback' => array($this, 'updatePersonifyEndpointSelect'),
                'event' => 'mouseup',
                'progress' => [
                    'type' => 'throbber',
                    'message' => t('Fetching Endpoints...'),
                ],
            ],
        ];

        // Attach our css
        $form['#attached']['library'][] = 'feeds_personify/personify-fetcher-form';

        $form['#validate'] = array();

        return $form;
    }


    /**
     * Function to populate endpoint options from base endpoint
     * @param array $form
     * @param FormStateInterface $form_state
     * @return AjaxResponse
     */
    public function updatePersonifyEndpointSelect(array &$form, FormStateInterface $form_state){

        // Get Data Service Url from form
        $personifyDataServiceUrl = $form_state->getValue('fetcher_configuration')['personify_data_service_url'];

        // Get data from endpoint
        try {
            $client = \Drupal::httpClient();
            $response = $client->get($personifyDataServiceUrl,['http_errors' => false]);
            $data = (String)$response->getBody();
        } catch (RequestException $e) {
            watchdog_exception('feeds_personify', $e->getMessage());
        }

        // Parse XML
        $xml = new \SimpleXMLElement($data);
        $endpointOptions = array();
        foreach ($xml->workspace->children() as $collection) {
            $endpoint = ((Array)$collection->attributes()->href)[0];
            $endpointOptions[$endpoint] = $endpoint;
        }

        $endpointSelect['specific_endpoint'] = [
            '#type' => 'select',
            '#required' => TRUE,
            '#title' => $this->t('Specific Endpoint'),
            '#description' => $this->t('Select the endpoint (collection) you would like to use.<br/>The options will be updated when you fill out the data service field.'),
            '#options' => $endpointOptions,
            '#name' => 'fetcher_configuration[specific_endpoint]',
            '#validated' => TRUE,
        ];

        $ajaxResponse = new AjaxResponse();
        $renderer = \Drupal::service('renderer');
        $ajaxResponse->addCommand(new ReplaceCommand('.form-item-fetcher-configuration-specific-endpoint', $renderer->render($endpointSelect)));

        return $ajaxResponse;

    }

}
