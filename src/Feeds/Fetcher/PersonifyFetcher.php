<?php

namespace Drupal\feeds_personify\Feeds\Fetcher;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\feeds\Exception\EmptyFeedException;
use Drupal\feeds\FeedInterface;
use Drupal\feeds\Plugin\Type\ClearableInterface;
use Drupal\feeds\Plugin\Type\Fetcher\FetcherInterface;
use Drupal\feeds\Plugin\Type\PluginBase;
use Drupal\feeds_personify\Feeds\Result\PersonifyResult;
use Drupal\feeds\StateInterface;
use Drupal\feeds\Utility\Feed;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\RequestOptions;
use Symfony\Component\HttpFoundation\Response;
use GuzzleHttp\Client;

/**
 * Defines an HTTP fetcher.
 *
 * @FeedsFetcher(
 *   id = "personify",
 *   title = @Translation("Fetch From Personify"),
 *   description = @Translation("Downloads data from Personify."),
 *   form = {
 *     "configuration" = "Drupal\feeds_personify\Feeds\Fetcher\Form\PersonifyFetcherForm"
 *   },
 *   arguments = {"@http_client", "@cache.feeds_download", "@file_system"}
 * )
 */
class PersonifyFetcher extends PluginBase implements ClearableInterface, FetcherInterface {

  /**
   * The Guzzle client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $client;

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Drupal file system helper.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Constructs an UploadFetcher object.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin id.
   * @param array $plugin_definition
   *   The plugin definition.
   * @param \GuzzleHttp\ClientInterface $client
   *   The Guzzle client.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The Drupal file system helper.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, ClientInterface $client, CacheBackendInterface $cache, FileSystemInterface $file_system) {
    $this->client = $client;
    $this->cache = $cache;
    $this->fileSystem = $file_system;
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function fetch(FeedInterface $feed, StateInterface $state) {

    $sink = 'private://feeds/personify_feed_source__feed_id_'.$feed->id().'.xml';
    $sink = $this->fileSystem->realpath($sink);

    $response = $this->get($sink, $this->getCacheKey($feed));

    return new PersonifyResult($sink, $response->getHeaders());
  }

  /**
   * Performs a GET request.
   *
   * @param string $url
   *   The URL to GET.
   * @param string $sink
   *   The location where the downloaded content will be saved. This can be a
   *   resource, path or a StreamInterface object.
   * @param string $cache_key
   *   (optional) The cache key to find cached headers. Defaults to false.
   *
   * @return \Guzzle\Http\Message\Response
   *   A Guzzle response.
   *
   * @throws \RuntimeException
   *   Thrown if the GET request failed.
   *
   * @see \GuzzleHttp\RequestOptions
   */
  protected function get($sink, $cache_key = FALSE) {

    // Make sure we can write
    chmod(file_directory_temp(),0777);

    // Create a place to store downloaded endpoint
    $sinkResource = fopen($sink, 'w');

    // Make sure we can read it later
    chmod($sink,0777);

    // Sets the save location to pass to GuzzleHttp/Client object
    $options = [
        'sink' => $sinkResource,
        'auth' => [
            $this->configuration['username'],
            $this->configuration['password'],
        ]
    ];

    // Determine endpoint
    $requestEndpoint = $this->configuration['personify_data_service_url'];
    if($this->configuration['specific_endpoint']!='_select') {
        $requestEndpoint = $requestEndpoint . '/' . $this->configuration['specific_endpoint'];
    }

    try {
      $guzzleClient = new Client(['base_uri' =>$requestEndpoint]);
      $response = $guzzleClient->request('GET', $requestEndpoint, $options);
    }
    catch (RequestException $e) {
      $args = ['%site' => $url, '%error' => $e->getMessage()];
      throw new \RuntimeException($this->t('The feed from %site seems to be broken because of error "%error".', $args));
    }

    if ($cache_key) {
      $this->cache->set($cache_key, array_change_key_case($response->getHeaders()));
    }

    return $response;
  }

  /**
   * Returns the download cache key for a given feed.
   *
   * @param \Drupal\feeds\FeedInterface $feed
   *   The feed to find the cache key for.
   *
   * @return string
   *   The cache key for the feed.
   */
  protected function getCacheKey(FeedInterface $feed) {
    return $feed->id() . ':' . hash('sha256', $feed->getSource());
  }

  /**
   * {@inheritdoc}
   */
  public function clear(FeedInterface $feed, StateInterface $state) {
    $this->onFeedDeleteMultiple([$feed]);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'personify_data_service_url' => FALSE,
      'username' => FALSE,
      'password' => FALSE,
      'specific_endpoint' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function onFeedDeleteMultiple(array $feeds) {
    foreach ($feeds as $feed) {
      $this->cache->delete($this->getCacheKey($feed));
    }
  }

}
